# Cybears Base Images

This repository is a collection of container images we use to build and deploy
CTF challenges. We don't currently include any of our Windows images here due
to licensing issues with redistributing images based on them.

The repo is broken up into two main categories:
  * `builders/` which contains images used to build challenges from source, and
  * `services/` which contains images used to run challenges in the cyber cloud

## On crossbuilds

We have a couple of crossbuilding images, but it would be generally preferable
to avoid spending time to creating and maintaining those images when
alternatives like dockcross [^dockcross] exist. Before deciding to implement
new crossbuild images, consider whether one of the existing dockcross images
(or something similar) would be a better choice.

We do aim to provide our own qemu-user based emulation containers to avoid
using the emulators included with dockcross, since we don't want to give
players access to handy gadgets which might allow them to trivially build evil
code on our infra. This is also (and probably more usefully) done as a way to
reduce overall container image size.

## On `socat` and `nsjail`

We historically used `socat` quite heavily and continue to provide these images
so that our older event repositories continue to function. However, we're
aiming to mostly use `nsjail` going forward as it provides better per-process
sandboxing, at the expense of the container itself needing to run with some
privileges.

We take inspiration from `kctf` [^kctf] and the Google CTF sources
[^google-ctf] for a lot of our use of `nsjail`. We'd recommend you do the same
if you choose to use any of our images or roll your own based on them.

## References

[^kctf]: https://github.com/google/kctf
[^google-ctf]: https://github.com/google/google-ctf
[^dockcross]: https://github.com/dockcross/dockcross
