#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

readarray -t NSJAIL_TMPMNTS < "/.nsjail_tmpfs"
readarray -t NSJAIL_BIND_RO < "/.nsjail_bind"
readarray -t NSJAIL_ENVVARS < "/.nsjail_env"

# Work out if the `noob` (or otherwise named) user actually exists
if ! id "${SANDBOX_USER:=noob}" &>/dev/null; then
    echo "WARNING: USER ${SANDBOX_USER} NOT PRESENT. Running as ${USER} instead"
    SANDBOX_USER="${USER}"
fi
: "${SANDBOX_GROUP:=nogroup}"

# Work out if we have an `/entrypoint.sh` script which should be called rather
# than the `BINARY` environment variable - we concretise the entrypoint now
# since it'll be running in a sanitised environment
if [ -x "${ENTRYPOINT_PATH:=/entrypoint.sh}" ]; then
    echo "Concretising the entrypoint script"
    /envsubst -no-digit -no-unset <"${ENTRYPOINT_PATH}" | tee "${ENTRYPOINT_PATH}.z"
    chown --reference="${ENTRYPOINT_PATH}" "${ENTRYPOINT_PATH}.z"
    chmod --reference="${ENTRYPOINT_PATH}" "${ENTRYPOINT_PATH}.z"
    NSJAIL_CHILD_CMD="${ENTRYPOINT_PATH}.z"
else
    NSJAIL_CHILD_CMD="${BINARY}"
fi

declare -ar NSJAIL_OPTIONS=(
    "--user=${SANDBOX_USER}:${SANDBOX_USER}" "--group=${SANDBOX_GROUP}:${SANDBOX_GROUP}"
    "--rlimit_as=8192"
    "${NSJAIL_TMPMNTS[@]/#/--tmpfsmount=}"
    "${NSJAIL_BIND_RO[@]/#/--bindmount_ro=}"
    "--bindmount_ro=${NSJAIL_CHILD_CMD}"
    ${BINARY:+--bindmount_ro="${BINARY}"}
    "${NSJAIL_ENVVARS[@]/#/--env=}"
)

# We refuse to die after this point
set +o errexit

# If specified sqet up a daemon nsjail that will echo OK. 
# This is for k8s and load balancer healthchecks because some of our jailed programs are expensive to run (qemu)
if [ ! -z "${BIND_HEALTHCHECK_PORT:-}" ]; then
    "${NSJAIL_PROG:=/nsjail}"                                           \
        --bindhost "${BIND_ADDR:-::}" --port $BIND_HEALTHCHECK_PORT     \
        "${NSJAIL_OPTIONS[@]}"                                          \
        -d                                                              \
        -- "/usr/bin/echo" "OK"
fi
# Actually run the nsjail
"${NSJAIL_PROG:=/nsjail}"                                               \
    --bindhost "${BIND_ADDR:-::}" --port "${BIND_PORT:-2323}"           \
    "${NSJAIL_OPTIONS[@]}"                                              \
    --symlink "/proc/self/fd:/dev/fd"                                   \
    -- "${NSJAIL_CHILD_CMD}"

# If nsjail as exited then kill the daemon one so this pod will be torn down.
if [ ! -z "${BIND_HEALTHCHECK_PORT:-}" ]; then
    pkill ${NSJAIL_PROG:=/nsjail}
fi
