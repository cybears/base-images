#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

# If the first argument is a valid command to run, do a passthrough exec
if [ $# -gt 0 ] && command -v "${1}" &>/dev/null; then
    exec "${@}"
# Otherwise check for a custom entrypoint from a downstream source
elif [ -x "${ENTRYPOINT_PATH:-/entrypoint.sh}" ]; then
    exec "${ENTRYPOINT_PATH:-/entrypoint.sh}" "${@}"
# Or finally a `BINARY` to run
elif [ -n "${BINARY:+SET}" ]; then
    exec ${BINARY:-/bin/false}  # No quotes to allow janky arguments
fi
echo "[E] No command specified" >&2
exit 127
