FROM debian

ENV DEBIAN_FRONTEND="noninteractive"
RUN apt update                                                              \
 && apt install -y sagemath                                                 \
 && rm -rf /var/cache/apt/*

# sage installed with APT needs the following paths bound under nsjail to run
RUN cat >>/.nsjail_bind <<EOF
/usr/share/sagemath/
/usr/share/singular/
EOF

ARG SAGE_HOME="/home/sage"
RUN useradd -d "${SAGE_HOME}" -m sage

RUN echo "/home/sage" >> /.nsjail_tmpfs
RUN echo "HOME=/home/sage" >> /.nsjail_env
# This ensures that scripts in the default path can be mounted into nsjail
RUN chmod a+X "/home/sage"

ENV USER="sage"
ENV SANDBOX_USER="${USER}"
USER "${USER}"
ENV HOME="${SAGE_HOME}"
WORKDIR "${SAGE_HOME}"

ENV ENTRYPOINT_PATH="/entrypoint-sage.sh"
# `--chmod` requires BuildKit
COPY --chmod=555 ./entrypoint.sh "${ENTRYPOINT_PATH}"
ENTRYPOINT ["/entrypoint-sage.sh"]
