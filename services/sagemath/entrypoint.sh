#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

# If the first argument is a valid command to run, do a passthrough exec
if [ $# -gt 0 ] && command -v "${1}" &>/dev/null; then
    exec "${@}"
# Otherwise check for a custom entrypoint from a downstream source - note that
# it is `SAGE_ENTRYPOINT_PATH` here to deconflict with this script which is
# itself set as the `ENTRYPOINT_PATH` for wrappers like socat/nsjail
elif [ -x "${SAGE_ENTRYPOINT_PATH:-/entrypoint.sh}" ]; then
    exec "${SAGE_ENTRYPOINT_PATH:-/entrypoint.sh}" "${@}"
# Or a sage script, special behaviour for this image type
elif [ -e "${SAGE_SCRIPT:-/run.sage}" ]; then
    exec "$(command -v sage)" "${SAGE_SCRIPT:-/run.sage}"
# Or finally a `BINARY` to run
elif [ -n "${BINARY:+SET}" ]; then
    exec ${BINARY:-/bin/false}  # No quotes to allow janky arguments
fi
echo "[E] No command specified" >&2
exit 127
