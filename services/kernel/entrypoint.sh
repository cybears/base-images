#!/usr/bin/env bash
cd ${WORKDIR}
"${QEMU_PROG:=/qemu-system-x86_64}"          \
    -m ${QEMU_MEMORY:=64M}                \
    -smp ${QEMU_CPUS:=1}                \
    -kernel bzImage     \
    -append "console=ttyS0 ${KERNEL_OPTIONS} panic=-1 quiet" \
    -initrd initramfs.cpio.gz \
    -monitor /dev/null \
    -no-reboot \
    -snapshot \
    -nographic
