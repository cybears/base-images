#!/usr/bin/env bash
"${QEMU_PROG}"                                                              \
    -L "${QEMU_LD_PREFIX:-/}" -cpu "${QEMU_CPU:-max}"                       \
    -R "${QEMU_RESERVED_VA:-1G}"                                            \
    -- "${@:-$BINARY}"
