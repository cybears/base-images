#!/usr/bin/env bash
set -o nounset
set -o pipefail
set -o errexit

SOCAT_TCPLFLAGS=",nodelay,reuseaddr,fork,${SOCAT_FLAGS:-}"
SOCAT_EXECFLAGS=",stderr,${SOCAT_EXECFLAGS:-}"

# Work out if the `noob` (or otherwise named) user actually exists
if id "${SANDBOX_USER:=noob}" &>/dev/null; then
    SOCAT_TCPLFLAGS="${SOCAT_TCPLFLAGS},su=${SANDBOX_USER}"
else
    echo "WARNING: USER ${SANDBOX_USER} NOT PRESENT. Running as ${USER} instead"
fi

# Work out if we have an `/entrypoint.sh` script which should be called rather
# than the `BINARY` environment variable - we concretise the entrypoint now
# since it'll be running in a sanitised environment
if [ -x "${ENTRYPOINT_PATH:=/entrypoint.sh}" ]; then
    echo "Concretising the entrypoint script"
    /envsubst -no-digit -no-unset <"${ENTRYPOINT_PATH}" | tee "${ENTRYPOINT_PATH}.z"
    chown --reference="${ENTRYPOINT_PATH}" "${ENTRYPOINT_PATH}.z"
    chmod --reference="${ENTRYPOINT_PATH}" "${ENTRYPOINT_PATH}.z"
    SOCAT_CHILD_CMD="${ENTRYPOINT_PATH}.z"
else
    SOCAT_CHILD_CMD="${BINARY}"
fi

# We refuse to die after this point
set +o errexit
# Actually run the socat loop
CLEAN_ENV_CMD="/usr/bin/env -i /dotenv -f /.env --"
while true; do
    "${SOCAT_PROG:=/socat}" -d -d                                           \
        "TCP-LISTEN:${SOCAT_TCPLPORT:-2323},${SOCAT_TCPLFLAGS}"             \
        "EXEC:${CLEAN_ENV_CMD} ${SOCAT_CHILD_CMD},${SOCAT_EXECFLAGS}"
done
