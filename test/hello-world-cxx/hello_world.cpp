#include <iostream>

int main() {
    std::ios::sync_with_stdio(true);
    setvbuf(stdin, 0LL, 2LL, 0LL);
    setvbuf(stdout, 0LL, 2LL, 0LL);
    std::cout << "Hello World!\n";
    return 0;
}
