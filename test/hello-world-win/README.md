# hello-world-win

This is a test container for testing that windows appjaillauncher can be
deployed to kubernetes. It provides a simple example flag and healthcheck
container.

We continue to use a flag.txt to demonstrate that appjaillauncher can properly
sandbox access to that file and expose it to a remote player.
